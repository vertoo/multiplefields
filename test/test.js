/**
 * Created by PhpStorm.
 * User: Grzegorz Gąsak (info@vertoo.pl)
 * Date: 10.07.14
 */

module("Test multipleFields plugin");

/*=====================================================================
*  test podstawowy  +requiredGroups
===================================================================== */
test('podstawowy +requiredGroups', function () {
    var d1 = $('#demo');
    d1.multipleFields({
        tmpl: $('#tmpl-demo'),
        itemsWrapper: '.items',
        inputWrapper: '.field',
        requiredGroups: 1
    });

    equal(d1.find('.fields-panel').length, 1, "Powinna być jedna grupa formularza");

    d1.data('multipleFields').addItem({value: 'test'});

    equal(d1.find('.fields-panel').length, 2, "Dodanie grupy formularza programistycznie");
});


/*=====================================================================
*  test podstawowy  +optionalGroups
===================================================================== */
test('podstawowy +optionalGroups', function () {
    var d1a = $('#demo1a');
    d1a.multipleFields({
        tmpl: $('#tmpl-demo'),
        itemsWrapper: '.items',
        inputWrapper: '.field',
        optionalGroups: 1
    });

    equal(d1a.find('.fields-panel').length, 1, "Powinna być jedna grupa formularza");
});


/*=====================================================================
*  test podstawowy kombinacja requiredGroups i optionalGroups
===================================================================== */
test('podstawowy kombinacja requiredGroups i optionalGroups', function () {
    var d1b = $('#demo1b');
    d1b.multipleFields({
        tmpl: $('#tmpl-demo'),
        itemsWrapper: '.items',
        inputWrapper: '.field',
        requiredGroups: 2,
        optionalGroups: 4
    });

    equal(d1b.find('.fields-panel').length, 4, "Powinno być 4 grup formularza");
});


/*=====================================================================
*  test metod
===================================================================== */
test('interakcje', function () {
    var d2 = $("#demo2");
    d2.multipleFields({
        tmpl: $('#tmpl-demo'),
        itemsWrapper: '.items',
        inputWrapper: '.field',
        requiredGroups: 1
    });

    var old = d2.find('.fields-panel').length;

    // dodanie nowego pola
    $(".btn-add-new-field2").on('click', function(e) {
        e.preventDefault();
        d2.data('multipleFields').addItem({value: 'test'});
    }).trigger('click');
    equal(d2.find('.fields-panel').length, (old+1), "Powinno być jedna grupa formularza");
    equal(d2.find('.fields-panel:last-child').find('input[name*="name"]').val(), 'test', "Pole powinno mieć wartość test");

    // sprawdzenie id
    var firstID = d2.find('.fields-panel:first-child').find('input').eq(0).attr('id');
        lastID = d2.find('.fields-panel:last-child').find('input').eq(0).attr('id');
    notEqual(firstID, lastID, 'ID pól formularza muszą być różne');

    // sprawdzanie label
    var firstL = d2.find('.fields-panel:first-child').find('label').eq(0).attr('for');
        lastL = d2.find('.fields-panel:last-child').find('label').eq(0).attr('for');
    notEqual(firstL, lastL, 'Label formularza muszą być różne');

    // usunięcie
    old = d2.find('.fields-panel').length;
    d2.find('.fields-panel:last-child').find('.btn-remove-field').trigger('click');
    equal(d2.find('.fields-panel').length, (old-1), "Po usunięciu grupy formularza musi być mniejsza grup o 1");

});
