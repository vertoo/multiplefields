# jQuery Plugin - multipleFields

### multiple form fields plugin

## Requirements

1. jQuery
2. Handlebars http://handlebarsjs.com/
3. jQueryUI Dialog (optional)

## HTML code

```
<script id="tmpl-demo" type="text/x-handlebars-template">
    <div class="fields-panel">
        <div class="fields-panel-body">
            <div class="field">
                <label for="field_0_name">Field name</label>
                <input type="text" name="field[0][name]" id="field_0_name" value="{{value}}">
            </div>
        </div>

        <div class="fields-panel-controls">
            <button class="btn yellow btn-remove-field">Remove</button>
        </div>
    </div>
</script>
```

## Usage

1. Include jQuery:

	```html
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
	```

2. Include plugin's code:

	```html
	<script src="dist/jquery.multipleFields.min.js"></script>
	```

3. Call the plugin:

	```javascript
	$("#element").multipleFields({
		propertyName: "a custom value"
	});
	```

#### [demo/](https://github.com/jquery-boilerplate/boilerplate/tree/master/demo)

Contains a simple HTML file to demonstrate your plugin.

#### [dist/](https://github.com/jquery-boilerplate/boilerplate/tree/master/dist)

This is where the generated files are stored once Grunt runs.

#### [src/](https://github.com/jquery-boilerplate/boilerplate/tree/master/src)

Contains the files responsible for your plugin, you can choose between JavaScript or CoffeeScript.

#### [.editorconfig](https://github.com/jquery-boilerplate/boilerplate/tree/master/.editorconfig)

This file is for unifying the coding style for different editors and IDEs.

> Check [editorconfig.org](http://editorconfig.org) if you haven't heard about this project yet.

#### [.gitignore](https://github.com/jquery-boilerplate/boilerplate/tree/master/.gitignore)

List of files that we don't want Git to track.

> Check this [Git Ignoring Files Guide](https://help.github.com/articles/ignoring-files) for more details.

#### [.jshintrc](https://github.com/jquery-boilerplate/boilerplate/tree/master/.jshintrc)

List of rules used by JSHint to detect errors and potential problems in JavaScript.

> Check [jshint.com](http://jshint.com/about/) if you haven't heard about this project yet.

#### [.travis.yml](https://github.com/jquery-boilerplate/boilerplate/tree/master/.travis.yml)

Definitions for continous integration using Travis.

> Check [travis-ci.org](http://about.travis-ci.org/) if you haven't heard about this project yet.

#### [multipleFields.jquery.json](https://github.com/jquery-boilerplate/boilerplate/tree/master/multipleFields.jquery.json)

Package manifest file used to publish plugins in jQuery Plugin Registry.

> Check this [Package Manifest Guide](http://plugins.jquery.com/docs/package-manifest/) for more details.

#### [Gruntfile.js](https://github.com/jquery-boilerplate/boilerplate/tree/master/Gruntfile.js)

Contains all automated tasks using Grunt.

> Check [gruntjs.com](http://gruntjs.com) if you haven't heard about this project yet.

#### [package.json](https://github.com/jquery-boilerplate/boilerplate/tree/master/package.json)

Specify all dependencies loaded via Node.JS.

> Check [NPM](https://npmjs.org/doc/json.html) for more details.


## License

[MIT License](http://zenorocha.mit-license.org/) © vertoo Grzegorz Gąsak
