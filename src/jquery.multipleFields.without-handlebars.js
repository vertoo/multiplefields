(function ($, window, document, undefined) {
    'use strict';
    // Create the defaults once
    var pluginName = 'multipleFields',
        defaults = {
            optionalGroups: 0,
            requiredGroups: 0,
            allowSorting: false,
            itemsWrapper: '.fields',
            inputWrapper: '.form-group',
            itemWrapper: '.fields-panel',
            itemsIgnore: '.select2-offscreen, .select2-input',
            btnAddField: '.btn-add-new-field',
            btnRemoveField: '.btn-remove-field',
            onAddItem: function () {},
            onEnumerate: function () {},
            onEnumerateElement: function () {},
            onBeforeRemove: function () { return true; },
            onRemove: function () {}
        };

    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, $.fn.multipleFields.defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {
        frm: null,
        wrapper: null,

        init: function () {
            var self = this,
                add_count = Math.max(self.settings.requiredGroups, self.settings.optionalGroups),
                i;
            this.frm = $(this.element).closest('form');

            if (add_count !== 0) {
                for (i = 0; i < add_count; i++) {
                    this.addItem();
                }
            }
            this.enumerate();

            $(this.element).on('click', this.settings.btnAddField, function (e) {
                e.preventDefault();
                self.addItem();
            });

            $(this.element).on('click', this.settings.btnRemoveField, function (e) {
                e.preventDefault();

                self.remove($(this).closest(self.settings.itemWrapper));
                self.enumerate();
            });

            if (this.settings.allowSorting) {
                $(this.element).find(this.settings.itemsWrapper).sortable({
                    placeholder: 'drag-placeholder',
                    axis: 'y',
                    handle: '.fields-panel-handle',
                    update: function() {
                        self.enumerate();
                    }
                });
            }
        },

        remove: function (item) {
            this.removeItem = item;
            if (this.settings.onBeforeRemove.call(this) !== false) {
                this.removeField(item);
            } else {
                return false;
            }
        },

        removeField: function(item) {
            item.remove();
            this.settings.onRemove.call(this);
            this.enumerate();
        },

        clearWrapper: function () {
            $(this.element).find(this.settings.itemsWrapper).empty();
        },

        enumerate: function () {
            var self = this,
                elements = $(this.element).find(this.settings.itemsWrapper).find('>div');
            elements.each(function (i, el) {
                $(el).find(':input:not(:submit, button)').not(self.settings.itemsIgnore).each(function () {
                    var old_id = $(this).attr('id'), id;
                    if (typeof old_id !== 'undefined') {
                        id = old_id.replace(/_(\d+)_/, '_' + i + '_');
                        $(el).find('label[for="' + old_id + '"]').attr('for', id);
                        $(this).attr('id', id);
                    }
                    $(this).attr('name', $(this).attr('name').replace(/\[(\d+)\]/, '[' + i + ']'));
                    self.settings.onEnumerateElement.call(this, i);
                });
            });
            if (self.settings.requiredGroups > 0) {
                if (elements.length <= self.settings.requiredGroups) {
                    elements.find('.btn-remove-field').hide();
                } else {
                    elements.find('.btn-remove-field:hidden').show();
                }
            }
            this.settings.onEnumerate.call(this);
        },

        addItem: function (items_data) {
            var item_content;
            item_content = $(this.element).find(this.settings.itemsWrapper).find(this.settings.itemWrapper).clone();
            $(this.element).find(this.settings.itemsWrapper).append(item_content);
            this.enumerate();

            this.newItem = $(this.element).find(this.settings.itemsWrapper).find('>*:last-child');
            this.settings.onAddItem.call(this);
        }
    });

    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        this.each(function () {
            if (!$.data(this, pluginName)) {
                $.data(this, pluginName, new Plugin(this, options));
            }
        });

        // chain jQuery functions
        return this;
    };

})(jQuery, window, document);
